---
title: "CI/CD Next.js SSR приложения"
date: 2020-05-22T12:05:42+03:00
draft: false
categories:
- Devops
tags:
- aws
- frontend
- gilab-ci
---

Технологии не стоят на месте, и JavaScript уже требуется запускать не на стороне клиента, а на сервере.
Так появляется технология Server Side Rendering (SSR), призванная не нагружать компьютер пользователя, и оптимизировать SEO выдачу. 

## Какие изменения это несёт для DevOps инженера? 
1. Нельзя всё сбилдить в статику. Если раньше React.js можно было скомпилировать в статичные файлы и загрузить на CDN, то теперь необходимо иметь отдельно и статическую и динамическую часть.
2. Нельзя всё кешировать. Кеш перед страницами сайта приведёт к сбоям в работе сайта в целом.
3. Как итог предыдущих пунктов - необходимость разного деплоя разных частей приложения.

## Общая схема
![AWS Nextjs AWS deployment](AWS_Nextjs_SSR_ECS_CF.png)

На схеме выше приведён пример деплоя [Next.js](https://nextjs.org/) приложения в облако AWS.

Как мы видим запросы на основной домен https://example.com попадают напрямую в балансировщик нагрузки и приложение в контейнерах ECS за ним.
При этом запрос статических файлов вынесен на домен https://static.example.com который отправляет запросы в Content Delivery Network (CDN) Cloudfront с файлами в S3 хранилище.

!!! Важно, чтобы ваш фреймворк поддерживал конфигурацию CDN префиксов для статических файлов. В Next.js слава богу это есть. Читаем в документации - [CDN Support with Asset Prefix](https://nextjs.org/docs/api-reference/next.config.js/cdn-support-with-asset-prefix).

```js {linenos=table}
module.exports = withSass({
  env: {
    ASSETS_CDN: process.env.ASSETS_CDN,
  },
  assetPrefix:
    process.env.NODE_ENV === 'production' ? process.env.ASSETS_CDN : '',
  },
});
```

## CI/CD
Собственно сама сборка/доставка приложения на примере Gitlab CI.

Для build и deploy стадии использован заранее подготовленный docker образ [docker-aws-nodejs](https://github.com/gsix/docker-aws-nodejs/blob/master/Dockerfile), который позволяет собирать nodejs приложение, упаковывать его в докер, и деплоить через aws-cli консоль.

Содержание `gitlab-ci.yml` максимально приближенного к боевому ниже.

### CI сборка
```yml {linenos=table}
build:
  stage: build
  image: genesix/docker-aws-nodejs:latest
  services:
    - docker:19.03.0-dind
  before_script:
    # Если делаете docker push например в AWS ECR то нужно залогиниться в консоли
    - aws ecr get-login --no-include-email --region eu-central-1 
  script:
    - echo "Building static files"
    - yarn && yarn build
    - echo "Building application image"
    - docker pull $REPOSITORY_URL:latest || true
    - docker build -f Dockerfile --cache-from $REPOSITORY_URL:latest -t $REPOSITORY_URL:latest
      --build-arg ASSETS_CDN=$ASSETS_CDN .
    - docker push $REPOSITORY_URL:latest
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
  artifacts:
    paths:
      - .next/static
```
Вся суть в билде (`yarn && yarn build`), выносе статичных артефактов (`- .next/static`) в следующую стадию деплоя и упаковке всего приложения в docker. 

### CD доставка

```yml {linenos=table}
.deploy_aws:
  stage: deploy
  services:
    - docker:19.03.0-dind
  image: genesix/docker-aws-nodejs:latest
  when: manual
  before_script:
    - aws ecr get-login --no-include-email --region eu-central-1
  script:
    - aws ecs update-service --force-new-deployment --service $SERVICE_NAME
      --task-definition $SERVICE_NAME --cluster $CLUSTER_NAME
    - aws s3 sync ./.next/static s3://$S3_BUCKET_NAME/_next/static --cache-control max-age=86400 --delete

deploy_stage:
  extends: .deploy_aws
  only:
    - develop
  variables:
    CLUSTER_NAME: project
    SERVICE_NAME: front
    S3_BUCKET_NAME: static.example.com
```
Деплой по сути идёт в две строки. Первая это вынудить сервис ECS сделать новый деплоймент.
Вторая это синхронизировать файлы в S3 корзине с последними артефактами и установить параметры кеширования. Параметр `--delete` при синхронизации удалит старые файлы. 

CI/CD систем существует большое множество, но в каждой из них можно применить похожие bash-скрипты =)


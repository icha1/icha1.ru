---
date: "2020-05-30T20:35:00-04:00"
draft: false
title: "Шахунов Василий Сергеевич"
url: "/about-me"
categories:
- me
#tags:
#- tag1
toc: false
---

## Middle Devops Engineer
*по итогам собеседований в EPAM и Clearscale*

![AWS Nextjs AWS deployment](vasiliy_shakhunov.jpg)

Больше всего в работе Devops инженера нравится приведение инфраструктуры из хаоса к порядку: инфраструктура-как-код, докеризация приложений, автоматический деплой и оркестрация контейнеров.

Мой стек:
* Clouds - AWS, DO, vscale, hetzner
* CI/CD - gitlab ci, aws code pipeline
* Infra-as-Code - Terraform, Chef, Ansible
* Containers - docker, packer
* Orchestration - Docker Swarm, AWS ECS
* Monitoring - Prometheus, Grafana, Zabbix
* Development - nodejs graphql serverless api on amazon lambda